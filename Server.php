<?php

class Lighthouse_Server
{
	private $_db;
	private $_connected = false;

	private function loadDb()
	{
		$config = include("game_config.php");
		$db = Zend_Db::factory('Pdo_Mysql', $config);

		switch (get_class($db))
		{
			case 'Zend_Db_Adapter_Mysqli':
				$db->getConnection()->query("SET @@session.sql_mode='STRICT_ALL_TABLES'");
				break;
			case 'Zend_Db_Adapter_Pdo_Mysql':
				$db->getConnection()->exec("SET @@session.sql_mode='STRICT_ALL_TABLES'");
				break;
		}

		return $db;
	}

	private function setup()
	{
		if ($this->_connected)
			return true;

		$this->_db = $this->loadDb();
		$this->_connected = true;
	}

	// Grab all bans
	public function getBans()
	{
		$this->setup();
		$bans = $this->_db->fetchAll('SELECT _SteamName, _UnbanTime, _Reason FROM bans');

		return $bans;
	}

	// Grab literally every character
	public function getCharacters()
	{
		$this->setup();
		$characters = $this->_db->fetchAll('SELECT * FROM characters');

		return $characters;
	}

	// Grab a character
	public function getCharacter($key)
	{
		$this->setup();
		$character = $this->_db->fetchAll('SELECT * FROM characters WHERE _Key = ?', $key);

		return $character;
	}

	// Just sorts some json shit out, fuck json in databases
	public function prepareCharacters($characters)
	{
		if ($characters)
		{
			$final = array();
			foreach ($characters as $character)
			{
				$character['_Data'] = json_decode($character['_Data'], true);
				$character['__PhysDesc'] = $character['_Data']['PhysDesc'];

				$final[] = $character;
			}

			return $final;
		}
	}

	// Grabs a list of the players characters + characters they recognise.
	public function getRecognisedCharacters($player)
	{
		$this->setup();
		$player_characters = $this->_db->fetchAll('SELECT * FROM characters WHERE _SteamID = ?', $player);

		$recognised = array();

		foreach ($player_characters as $character)
		{
			$recognisedCharacters = json_decode($character['_RecognisedNames'], true);

			$recognised = array_merge($recognised, $recognisedCharacters);
		}

		$people = $this->getCharacter($recognised);
		$people = array_merge($people, $player_characters);

		$people = $this->prepareCharacters($people);

		return $people;
	}
}