<?php

class Lighthouse_Pages_StaffPage
{
    public static function respond(XenForo_ControllerPublic_Abstract $controller, XenForo_Controllerresponse_Abstract $response)
    {
        $userModel = $controller->getModelFromCache('XenForo_Model_User');
        $usergroupModel = $controller->getModelFromCache('XenForo_Model_UserGroup');

        $seniorCommunityAdmins = array();
        $communityAdmins = array();
        $superAdmins = array();
        $admins = array();
        $devs = array();

        $SCA = $usergroupModel->getUserIdsInUserGroup(3);

        foreach ($SCA as $id=>$primary)
        {
            $user = $userModel->getFullUserById($id);
            $user = $userModel->prepareUser($user);
            $seniorCommunityAdmins[] = $user;
        }

        $CA = $usergroupModel->getUserIdsInUserGroup(7);

        foreach ($CA as $id=>$primary)
        {
            $user = $userModel->getFullUserById($id);
            $user = $userModel->prepareUser($user);
            $communityAdmins[] = $user;
        }

        $SA = $usergroupModel->getUserIdsInUserGroup(9);

        foreach ($SA as $id=>$primary)
        {
            $user = $userModel->getFullUserById($id);
            $user = $userModel->prepareUser($user);
            $superAdmins[] = $user;
        }

        $A = $usergroupModel->getUserIdsInUserGroup(8);

        foreach ($A as $id=>$primary)
        {
            $user = $userModel->getFullUserById($id);
            $user = $userModel->prepareUser($user);
            $admins[] = $user;
        }

        $DEVS = $usergroupModel->getUserIdsInUserGroup(5);

        foreach ($DEVS as $id=>$primary)
        {
            $user = $userModel->getFullUserById($id);
            $user = $userModel->prepareUser($user);
            $devs[] = $user;
        }

        $response->params['seniorCommunityAdmins'] = $seniorCommunityAdmins;
        $response->params['communityAdmins'] = $communityAdmins;
        $response->params['superAdmins'] = $superAdmins;
        $response->params['admins'] = $admins;
        $response->params['devs'] = $devs;

        $response->templateName = 'lighthouse_staff';
    }
}