<?php

class Lighthouse_Pages_TimelinePage
{
    public static function respond(XenForo_ControllerPublic_Abstract $controller, XenForo_Controllerresponse_Abstract $response)
    {
        $response->templateName = 'lighthouse_timeline';
    }
}