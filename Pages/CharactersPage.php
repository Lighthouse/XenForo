<?php

class Lighthouse_Pages_CharactersPage
{
	public static function respond(XenForo_ControllerPublic_Abstract $controller, XenForo_Controllerresponse_Abstract $response)
	{
		$lhServer = new Lighthouse_Server();
		$steam = new Steam_Helper_Steam;
		$userModel = $controller->getModelFromCache('XenForo_Model_User');

		$cVisitor = XenForo_Visitor::getInstance();

		$user = $userModel->getFullUserById($cVisitor->user_id);
		$user = $userModel->prepareUser($user);

		$steamid = @unserialize($user['external_auth'])['steam'];

		if (!$steamid || $steamid == '')
		{
			$response->params['error_title'] = 'Steam not associated';
			$response->params['error_desc'] = 'Before you can continue, you have to associate your forum profile with Steam.';
			$response->templateName = 'lighthouse_error';
		} else {
			$characters = $lhServer->getRecognisedCharacters($steam->convertIdToString($steamid));

			$response->params['characters'] = $characters;

			$response->templateName = 'lighthouse_characters';
		}
	}
}