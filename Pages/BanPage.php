<?php

class Lighthouse_Pages_BanPage
{
    public static function respond(XenForo_ControllerPublic_Abstract $controller, XenForo_Controllerresponse_Abstract $response)
    {
        Lighthouse_Server::setup();
        $bans = Lighthouse_Server::getBans();

        $response->params['bans'] = $bans;
        $response->params['time'] = time();

        $response->templateName = 'lighthouse_bans';
    }       
}